package com.heartwork.android.ui;

import java.util.List;

import com.heartwork.android.R;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MainActivity extends FragmentActivity implements
		OnCheckedChangeListener {
	private Fragment[] mFragments;
	private RadioGroup tabMain;
	private FragmentManager fragmentManager;
	private FragmentTransaction fragmentTransaction;
	private LocationManager locationManager;
	private String locationProvider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initView();
	}

	LocationListener locationListener = new LocationListener() {

		@Override
		public void onLocationChanged(Location location) {
			// 如果位置发生变化,重新显示
			showLocation(location);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
		}

	};

	private void initView() {
		// 获取地理位置管理器
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		List<String> providers = this.locationManager.getProviders(true);
		if (providers.contains(LocationManager.GPS_PROVIDER)) {
			// 如果是GPS
			this.locationProvider = LocationManager.GPS_PROVIDER;
		} else if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
			// 如果是Network
			this.locationProvider = LocationManager.NETWORK_PROVIDER;
		} else {
			Toast.makeText(this, "没有可用的位置提供器", Toast.LENGTH_SHORT).show();
			return;
		}
		// 获取Location
		Location location = locationManager
				.getLastKnownLocation(locationProvider);
		if (location != null) {
			// 不为空,显示地理位置经纬度
			showLocation(location);
		}
		// 监视地理位置变化
		locationManager.requestLocationUpdates(locationProvider, 3000, 1,
				locationListener);

		this.fragmentManager = this.getSupportFragmentManager();
		this.tabMain = (RadioGroup) this.findViewById(R.id.main_tab);
		this.tabMain.setOnCheckedChangeListener(this);
		this.mFragments = new Fragment[5];
		this.mFragments[0] = fragmentManager.findFragmentById(R.id.tab_main);
		this.mFragments[1] = fragmentManager.findFragmentById(R.id.tab_shake);
		this.mFragments[2] = fragmentManager.findFragmentById(R.id.tab_heroes);
		this.mFragments[3] = fragmentManager.findFragmentById(R.id.tab_armors);
		this.mFragments[4] = fragmentManager.findFragmentById(R.id.tab_more);
		this.fragmentTransaction = this.fragmentManager.beginTransaction()
				.hide(mFragments[0]).hide(mFragments[1]).hide(mFragments[2])
				.hide(mFragments[3]).hide(mFragments[4]);
		this.fragmentTransaction.show(mFragments[0]).commit();

	}

	private void showLocation(Location location) {
		// TODO Auto-generated method stub
		String locationStr = "维度：" + location.getLatitude() + "\n" + "经度："
				+ location.getLongitude();
		Toast.makeText(MainActivity.this, locationStr, Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (locationManager != null) {
			// 移除监听器
			locationManager.removeUpdates(locationListener);
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		this.fragmentTransaction = this.fragmentManager.beginTransaction()
				.hide(mFragments[0]).hide(mFragments[1]).hide(mFragments[2])
				.hide(mFragments[3]).hide(mFragments[4]);
		switch (checkedId) {
		case R.id.radio_button0:
			this.fragmentTransaction.show(mFragments[0]).commit();
			Toast.makeText(MainActivity.this, R.string.app_message_alert,
					Toast.LENGTH_SHORT).show();
			break;
		case R.id.radio_button1:
			this.fragmentTransaction.show(mFragments[1]).commit();
			Toast.makeText(MainActivity.this, "开发ing.....", Toast.LENGTH_SHORT)
					.show();
			break;
		case R.id.radio_button2:
			this.fragmentTransaction.show(mFragments[2]).commit();
			Toast.makeText(MainActivity.this, "开发ing.....", Toast.LENGTH_SHORT)
					.show();
			break;
		case R.id.radio_button3:
			this.fragmentTransaction.show(mFragments[3]).commit();
			Toast.makeText(MainActivity.this, R.string.app_message_alert,
					Toast.LENGTH_SHORT).show();
			break;
		case R.id.radio_button4:
			this.fragmentTransaction.show(mFragments[4]).commit();
			Toast.makeText(MainActivity.this, R.string.app_message_alert,
					Toast.LENGTH_SHORT).show();
			break;
		default:
			Toast.makeText(MainActivity.this, R.string.app_error_message,
					Toast.LENGTH_SHORT).show();
			break;
		}
	}

}
